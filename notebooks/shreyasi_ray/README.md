# Progress Notebook

This personal notebook keeps track of the progress of the team, and specifically myself, in regards to the project, and every aspect is kept track of through this file. 


## 17th January, 2023

The team discussed various ideas, and each member wrote out points for each idea in order to formally be able to write out the initial post on the Web Board. 

My idea was the Hand-Cranked Charger, since it seemed like a very useful concept to have in times of emergency. It would allow users to save themselves in dire situations, and they would be able to generate power for their own device and make emergency phone calls or send texts as per their requirements. 

I had the idea since it seems like it would be very useful, and consumers would gain a lot of utility through this. But to market this product, it was essential to make sure there was maximum user comfort and the features would make it better than already existing products.

One of the key focal points in this discussion was the selection of components. We had specific criteria to consider, given the various requirements we had, such as affordability, efficiency, portability, and automation. Consequently, we had to carefully choose the components that met these requirements. Our objective was to find a motor that was lightweight and compact, ensuring it could fit into a small space. Additionally, we aimed for a motor with a sufficiently high voltage to reliably deliver a steady 5V output to the USB port. We aimed to design a customized hand crank that could be folded, reducing its overall length when not in use. Simultaneously, we wanted to maintain a significant distance between the rotation axis and the point where force is applied. This was important because by increasing the distance, we could decrease the amount of force the user would need to exert to achieve the desired torque output.

The team liked the idea and we went through with the initial post in order to discuss the idea further.

[Link to Initial Post for this idea](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=71835)

## 22nd January, 2023

The team had a meeting, and we decided which idea to post for formal approval, that is, the RFA. We took into consideration the advice given by the Professor and the TAs through replies to the initial posts. 

Two of our ideas were approved for the RFA stage, and the team discussed each thoroughly to check which idea is better to work on, and more viable. We talked about efficiency, affordability and other features of each product, along with design factors of the product itself. We were able to post our RFA and get it approved before the early deadline given. 

[Link to RFA for the idea](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=72429)

## 4th February, 2023

The team had a meeting to discuss the Project Proposal. After checking the requirements, we worked on the details of the block diagram, high-level requirements and the general introduction. 

We had to start considering the design element of the product in greater detail since the proposal would give a deeper view into the workings of the product. 

The general rough idea of the block diagram is attached.

![Rough Block Diagram](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/rough_block.png "Rough Block Diagram")

## 7th February, 2023

The team had the first meeting with a TA, and during this short discussion we learned about many things we needed to include while writing out the project proposal. 

We talked about various motors which would be suitable, while mentioning which portions of the product we wish to manually create ourselves. While giving brief descriptions of each of our components through the block diagram, we had a helpful discussion about the variable voltage divider. This is one of the most crucial portions of our design, and we realized we had to do more critical thinking about the high-level requirements before the project proposal was written up. 

We got our assigned locker, and discussed setting up future meetings with our designated TA.

## 9th February, 2023

The team had a meeting to discuss and finalize the project proposal before the deadline. We were able to work and visalize the CAD models for the design of the hand-crank generator product. 

![Physical Build](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/physical1.jpeg "Physical Build")

We had to go through manyh rough ideas for feasible designs for the product.  Several components were discussed so that we could later get confirmation on the models and whether we would be able to make such a product design.

Till this stage, the product development was very high-level. We were yet not decided on which exact components we wanted to use in order to be able to make the product viable and working. We were able to get the high-level requirements ready for the project, and what we wanted to target. Our block diagram seemed pretty feasible, however, we still needed to confirm the working of certain components in the diagram.

## 10th February, 2023

The team went to the machine shop to discuss the physical components of the product. We learnt about the various motors, and which sizes would be viable for the design. 

Since we did not want the generator to be very big, we asked for more portable size options and were given many suggestions as to what motors we could use. The machine shop lent us a motor to conduct testing on, and sketch out the realistic area and size of the product. The machine shop also helped us get ideas on how big the casing should be for the design to be viable. We were able to think of the rough placement of components in the casing, to make the most use of the main board which will have the main components mounted on it.
We were able to choose a 12V brushed motor since it provides the required output and it could be cranked at a reasonable rate. There was also a discussion about the rough size of the hand-crank, which would have to be long enough for efficient cranking. This meeting was able to give us our checkpoints for further discussion. To avoid electrocution and unwanted conduction, the machine shop also showed us plastic casings to help with safety regulations of the product. The rough components of the design was put together through this meeting, and it helped get an idea on what is viable for the design and what is not.

![Rough Design](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/rough_design.png "Rough Design")

## 16th February, 2023

I was able to finalize some parts of the design document. Some time had to be spent going over components, and which ones would be more suitable for the design. While this was being planned, the physical design also had to be thought of, and how to make it the most convenient for users. Since it was an emergency product, and that was the brand the team was trying to target, we wanted to aim to make it very comfortable to use, with the features being as automated as possible. The only constraints we were facing was budget, and we were trying to decide upon which parts to get and trying to work around getting reliable but affordable parts. 

![Schematic](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Schematic.png "Schematic")

## 21st February, 2023

The team worked together on the design document. We were able to incorporate the physical design ideas, and figure out most of the features. This is the document where the team was able to finalize a lot of the components we were expecting to use. Some of the components are: the ATMEGA328P Microcontroller, the Boost Converter, the Display, the Relay and the Power MUX. On deciding upon the battery, we also had to educate ourselves on the very important topic of battery safety. This also had to be included in the safety portion of the document, so I had to go through various sources in order to properly understand battery safety.

The team was also able to look into resistors and capacitors for the circuit. Since the Machine Shop had lent us the motor, we were able to use the budget on the other components. Through this document, we were able to draw up a rough schedule for who is assigned which task, to ensure smooth progress. It also gave us a timeline to follow, since we were scared to fall behind and not realize the work ahead of us. 

The Design Document was a very comprehensive report, and along with describing our product, it helped us have a gameplan for the process of building the project. We were able to gain enough insight to understand what to do, and how to go about designing the rest of the product. The document helped us figure out tolerances, and the limits of our components. It made us realize the importance of tesing, and we had to discuss briefly how we would test each part before finally building the circuit. This was when we could brainstorm and cover the gaps in knowledge, to provide a detailed report of what we expected our product would do.

The requirements and verification was a pretty thorough assignment we had to complete. This portion of the document took some time, because we had to think of methods of verification, and how to go about it in a concise manner, making sure we were not missing any key explanations. This helped make clear which parts of the circuit were the most important to target, and we were able to set achievable goals.

The design document gave us a very clear outline of what we needed to do ahead.

![Block Diagram](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Final%20Block%20Diagram.png "Block Diagram")

## 22nd February, 2023

The team met to finalize parts, and discuss which parts needed change. Tolerance analysis had to be imporved and we had to account for losses. The circuit had a diode, which has a voltage drop of 1V approximately, and hence, we needed to cover this loss by incorporating a steady and sufficient output at the linear regulator, which as per our requirements was 6V. This is when we were able to discuss the cost of the parts, and started looking for components online. This helped us draw up a bill of material to follow for our project. My teammates had a meeting with our mentor Jack Blevins, and they discussed the components. We were able to smooth out some irregularities with regards to functioning of the microcontroller, and how it would communicate with the display. We were able to decide on programming languages, and what we were the most comfortable and experienced with. 

There was a discussion regarding the careful examination of how many GPIO pins we could require to output to the 7-segment display. It generally requires 8 pins - 7 for the segments and 1 for the decimal point. This made us look for alternatives. Here is when we incorporated the I2C Backpack since it used less pins, it used 4 pins and the other could be used for other purposes.

The team also got to know more about how to incorporate the use of the Power MUX in the circuit, and how to make it more efficient. We also had to go over the working of the display, and how we would inform the user about cranking the product faster or slower to avoid any sort of damage to the circuit. We were also planning on starting to discuss PCB planning, since we wanted to get a headstart, before the first round of ordering began. This was the stage where we were polishing the design, and trying to figure out a viable method of implementation.


## 24th February, 2023

The team met with our TA, Matthew. He informed us about the gaps in our knowledge and how we could improve upon some points in the document. This would help better the status of the product, and make things clearer for us. We had to discuss in detail, working for a lot of components, and consider which was the best option for the circuit. Our tolerance analysis had to be more detailed for testing purposes, and we had to improve upon our high-level requirements. There was a discussion about capacitors, resistors and other components required. We learnt about how we could use an Opto Coupler for the signals to the Power MUXs and the Relay. Matthew warned us about the safety regulations of battery and how purchasing an off-the-shelf component would be the better option for us. This helped us gain a lot of insight into the parts, and we chose to use the Adafruit Powerboost 1000C. 

The team had to form a more detailed requirement and verification table in order to incorporate exact values for testing. We tried to incorporate more use cases for the product, to explore the validity of the features we were trying to implement. This is when we also realized that the budget would lead to more constraints in the future regarding purchasing. However, we were successfully able to form a final bill of material.

![TA Meeting 24th Feb 2023](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/TA_meeting_02-24.png "TA Meeting 02 24 23")

## 27th February, 2023

The Design Review was held today, with a team of TAs including Matthew. Our professor was Professor Olga, and we were presenting our design document. This was a very helpful assignment, and we were advised that our requirements and verifications have to be in the form of instructions. This is to ensure that even if someone does not know the integral details of the project, they would be able to conduct the testing smoothly.

The subsystems received good feedback, and we were asked to include more tolerance values in the document. The block diagram had also been designed to be more detailed, and our presentation went smoothly. The requirement and verification was the main part we were asked to modify, and since this was the first time our idea was getting critiqued, we were able to gain very necessary feedback. 

There were some other things that needed to be polished, however, the rest of the document was approved. The physical build of the product had to be finalized to report back to the machine shop. We had to take into account many safety considerations since we were working with a battery, and hence the material of the casing had to be solid to withstand any sort of external damage. We explained how the machine shop provided many reccomendations about where to find the right size and material for the case of the product. The product also had to be compact, since this would reduce the cost from the already existing products in the market that have very bulky and heavy features.

This was when much of the product description and circuit details were finalized and approved. We had the confidence to start working on a viable design. 

## 28th February, 2023

My teammates and I completed the PCB Schematic, and proceeded to work on the design. This needed a detailed discussion of where we would place each component on the board. It would have to be easy to debug, and we wanted to take some time with the design to avoid unnecessary future changes in the PCB. 

This would also help us have an idea of how much space this product would need internally, and how big the casing would be. We used KiCAD for the layout, and work on the traces. It seemed like a good idea to put each group of components (that serve a particular function) in a particular section on the board. 

Hence, we finalyl decided on the input system on the bottom, the output system to the middle left, the power system on the top and the programming system on the right of the PCB. Thus, the top left contained the USB Output, the botton left had the Motor and the USB Input, the top had the boost converter, the right had the programming and display, and lastly the middle consisted of the switching logic compoenents such as the microcontroller, the Power MUX and the Relay. This would help us understand where faults occur, if any, and we would not have to spend too much time on finding the fault and debugging would become much more simple.

We did encounter some problems during this process. Unfortunately, the Power MUX we were using, the TPS2116 from Texas Instruments, was too small. Due to the lack of sodlering experience in any other previous class, we were scared to work with such an intricate component. Even though the oven was another method we could use for this part, we wanted to look for a more convenient alternative. Thus, with some more research we came upon the TPS2115 from Texas Instruments. This was small still, but larger than the previous one. While the previous one was 2.4mm x 1.2 mm, this came to be 3mm x 6.4mm and seemed to be more viable for us. The pins were also comparatively longer, and would be easier to solder on the PCB. 
We had to update this component and have the first draft of our design. 

We wanted to get this done by the first round of PCB orders, since getting a headstart seemed like a better idea since we might need more revisions for which more orders would have to go through. 

## March 2nd, 2023

Testing the parts is also important, and we wanted to get the parts in as soon as possible. 

However, through this process, we encountered some issues. Many components we needed and had decided upon were out of stock, which meant we had to disucss new components. This took some extra time than we had expected, since it was difficult to find components with the same functionality. We would have to integrate and consider some extra features that came with the new parts. An example is, the Adafruit Powerboost 1000C was out of stock, and we had to find a replacement. We chose to move on with the battery module from MakerFocus. We realized this helped make our product more simple in terms of internal circuit. 

Initially, our plan for the hand-crank subsystem involved using two Power MUXes. The MUX1, controlled by a selector signal from the Microcontroller, would determine whether the device connected to the USB output should be charged through the hand-crank circuit or the battery. The MUX2, also controlled by a selector signal from the Microcontroller, would determine whether the battery is charged by the hand-crank or the Micro-USB Input.

However, after conducting further research, we discovered the TP4056, an off-the-shelf battery charging module. This component fulfilled the same functionality as MUX2, eliminating the need for two MUXes. This simplification of the system made the overall process more straightforward. Furthermore, using the TP4056 added an extra layer of safety as it handles battery management, making our product safer to use.

This process took a little longer than our expected schedule mentioned in the design document. However, we were able to implement the new components in the circuit, and work around the modified functionality than we had expected before. 

## March 6th, 2023

After thorough collaboration and effort, the team successfully completed the finalization of the PCB design. To ensure accuracy and reliability, we utilized the PCBWay online system for verification. This step was crucial in guaranteeing that no minor details were overlooked during the design process. We paid meticulous attention to every aspect, carefully considering the placement of components, routing of traces, and overall layout of the PCB. By taking these measures, we aimed to create a design that would not only meet the desired functionality but also optimize performance and manufacturability. The verification process served as a comprehensive check to confirm that our design adhered to the required specifications and industry standards.

Achyut was able to send the final files to Matthew, our TA. It was in time for the first round of orders for PCB. While the PCB was being shipped, we hoped to be done with finalizing and purchasing parts.

![PCB Without Traces](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Screenshot%202023-03-06%20124655.png "PCB without traces")

![PCB 3D](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/PCB_3D_AV.png "PCB")

## March 15th, 2023

During this Spring Break, our progress was slower than expected since all the team members could not meet. However, we were able to finalize components and get them ready for purchasing. This was taking more time than expected due to the components suddenly going out of stock from various vendors. It was taking some time to find alternative options.

## March 18th, 2023

The final components were decided upon by Achyut and I, and we were waiting on approval from Matthew.

![Bill of Materials](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/BillOfMaterials.png "Bill of Materials")

# March 22nd, 2023

This was when our orders went through, and Matthew approved them after a brief discussion on design choice, and whether they would be viable for the project.

During this discussion, various aspects were considered, including the compatibility of the new components with the existing design, their performance capabilities, and their overall feasibility in meeting the project's requirements. Matthew, with his deep understanding of the design choices and project goals, carefully evaluated the proposed components to ensure they aligned with the desired outcomes.

After the discussion, Matthew provided his approval for the selected components, signifying their compatibility and appropriateness for the project. 


## March 31st, 2023

The team had a meeting with with Matthew, for a simple progress check. However, we encountered an unexpected issue during this. We were informed that our resistors and capacitors were too small, and not of the size they needed to be according to our PCB design and footprints. 

The resistors we ordered were 0402, which were too difficult to be hand soldered, and hence we needed bigger ones. Some of the components were in fact 1206 which were also too big for the footprints on the PCB. The size we required was 0805, and we had not checked the sizing before.

This cost us some extra time, since the parts were already on their way. We had to find new components, of the right size, and place new orders from the vendors. Even though some time was lost, we were able to quickly get back on track. The focus shifted to expediting the process of finding the appropriate components and ensuring their availability. The collective efforts of the team members, with their dedication and effective communication, enabled us to swiftly adapt to the situation and get back on track without significant delay.

## April 2nd, 2023

By this time, the team was a little late on progress since the parts that we had ordered were still not delivered. We had placed a cart order on Digi-Key and it had not arrived. This contained the most important components of the circuit, and without them we found that we could not begin testing or soldering. 

I went to the Business Administration Department, since all the Purchasing orders were through my account. Nina Stadler and Beverly Curtis were extremely helpful in finding out some information about where our parts were and what the status was. They informed me that the whole cart shipment had been delayed due to some of the components being out of stock. This was delaying the shipment of the whole cart, and hence, we were not being able to get the delivery on time before the parts came into stock. 

This was causing quite some confusion since we were not able to get an estimated delivery date from the vendor. Nina helped me place new orders for the components that were in stock, and opted for expedited delivery. This would save us some time from waiting for the same components again. On the other hand, we had to figure out how to find new components for the ones Digi-Key could not deliver to us. New research had to be conducted again, and we had to once again start finalizing parts which would integrate well with our circuit. 

This caused a great setback for the team, since the parts were taking a long time to finally arrive, hence stalling our entire process with building of the product.

## April 4th, 2023

Achyut and I went to the machine shop in order to finalize the design, and make sure the physical build of the product was confirmed. 

We had earlier met with Gregg Bennett, and explained our design to him. He advised us on cases and lent us a motor to use for our product. We could have used a lighter and smaller motor, but since we needed to use the motor through very intricate testing, Gregg recommended a more reliable motor. He mentioned that in the prototype phase, it was best to first think about functionality and then take the time to modify it into a commercial product.

The machine shop recommended ABS plastic casing, so ensure protection for all inner components. The motor also had metal reinforcement since the torque of the motor would otherwise crack plastic material. The metal also helps keep the product steady while cranking.

We met with Skee Aldrich, and gave him a short outline of the project and what we were hoping to make. He gave us very helpful advice and his insight proved very beneficial to the product. We had to give them an empty PCB, without parts soldered on it. This was so that he could visualize the rough dimensions of the case. The machine shop had ordered many cases of various sizes and shapes. Even though the box we picked was much bigger than what we required, we thought it was better to go for a bigger box just in case we fell short on size for any particular reason. We gave Skee estimates and dimensions of our battery, the display and the other daughter baords. 

We had to consider the overall dimensions of the product, and along with that it was important to determine what the length of the hand-crank would be. Since our product was meant for emergencies, we needed to make it as comfortable to use as possible.

The first consideration to make was the hand-crank. It would have to be an ideal length, so that users can easily crank it without extreme force. We all tried different lengths, and decided that the crank could be 5 inches. 
At the end of the hand-crank, Skee recommended a rotatable tip since it would help the user crank the product without orienting their hands into awkward angles. It would help the users keep their hands steady.

Secondly, we wanted the display to be oriented towards the direction of the user while they are cranking. This is to enable the users to read the display in the right direction while cranking the product.

These are small additions to the design of the product, which make sure the user can comfortably use the product without having any issues while cranking. 

This helped us finalize the physical build with the machine shop, and they were extremely helpful and willing to incorporate all our demands into the project.

## April 5th, 2023

We went to the machine shop again, to drop off the PCB and send him the datasheets which he required for the parts. This helped him chalk out the dimensions of the components inside the product. 

The parts were delivered and we checked whether all the parts were delivered. Testing was done on the resistors and capacitors to ensure no faulty parts were used on the final circuit.
Later, we decided to go to lab and begin more testing and soldering. We were still waiting on some components such as the Power MUX and the Microcontroller. 

The Relay was working as we expected it to. My teammate Achyut was able to test it with the help of the Digital Multimeter. Furthermore, the device exhibited exceptional speed in its switching capabilities, particularly when current flows through the control pins. This rapid switching process effectively mitigated any potential ripple effects that could arise from slow or uncontrolled switching actions. It was tested at varying voltages of 3V and 6V, and made sure it was working. The Relay was plugged into a breadboard and the output voltage was checked according to the input signal. 
The relay has two types of outputs, one that is normally open and one that is normally closed, and it also has input rails and switching logic. We tested the relay by providing power to two specific pins, pin 1 and pin 6, using a voltage between 3V and 6V. When we did this, we found that pin 3, which is the normally closed output, received power with very little loss of voltage (less than 0.1V). Then we supplied power to pin 2 and connected the ground to pin 5. This time, we saw that pin 4, which is the normally open output, received power, while pin 3 did not receive any power.

## April 6th, 2023

After a brief meeting with Skee, we were able to receive our hand-crank charger screwed into the casing, as discussed. The motor had also thus been set into the casing. This was very good progress for the team, and we were able to go to the lab and start soldering components onto the PCB.

Resistors, capacitors, diodes, and more components were soldered onto the PCB. We also soldered the 7 segment display and the I2C Backpack. We left out the parts near the Power MUX since we wanted to make sure we dealt with the intricate soldering of that first, before targeting the surrounding parts. This was so that we had enough space for the small Power MUX, and we were able to solder it with comfort. It was the first time we would solder something so small.
To ensure proper connections during the soldering process, we conducted continuity checks using the Digital Multimeter. This involved verifying that electrical continuity existed between the relevant points, confirming that the desired connections were successfully established.

Most of the soldering was finished, and the team was back on track, making great progress with the design. 

The delivery of the microcontroller we originally intended to use was postponed due to it being back-ordered. Consequently, to avoid further delays, we made the decision to acquire an Arduino Uno Rev3. This particular model features a removable ATMega328p microcontroller, which aligns with our project requirements. By opting for the Arduino Uno Rev3, we were able to proceed with our development and testing without being hindered.

P.S. Our Digi-Key order had contained the Microcontroller and the Arduino, since the Microcontroller was taking a lot of time. However, the Arduino had arrived without the Microcontroller. The Business Administration Department had told us that the Microcontroller was not expedited since Digi-Key did not have that feature for this component. So, it would arrive on the 11th of April.

![Final Build](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Build.jpg "Final Machined Build")

## April 7th, 2023

The team met with Matthew, and updated him on the progress. He approved of the work we were doing, and this gave us a lot of confidence about the schedule and timeline goals. 

Now, we were planning on delegating work with Rubhav working on testing the PCB, while Achyut and I were working on programming the microcontroller. However, we were still waiting on the delivery of the mircocontroller. So, we wanted to start making the idea of the program and chalk up pseuodo-code so that when the part was delivered, we would be able to get the work done without wasting any time.

## April 10th, 2023

Achyut and I initiated the programming process for an Arduino, aiming to develop a foundational code that would enable convenient testing. To facilitate this, we connected the Arduino to a breadboard, allowing for easy experimentation and prototyping. Achyut was able to test the display and tried to experiment with how he would access pin values and output to them. 

The Business Administration Department had informed us that our Digi-Key order for the Microcontroller would arrive on the 11th of April. 

## April 12th, 2023

The team encountered a problem where there was no direct connection between the motor and the microcontroller. We were unsure of how to obtain the sense signal without risking a voltage higher than 6V. To address this, we initially planned to use resistors and wires to measure the voltage.

However, on the same day, we realized that this approach might disrupt the current flow and cause complications. As a solution, we decided to solder a wire from the output of the 5V linear regulator to an available analog pin on the microcontroller. This modification allows us to monitor and read the voltage value being generated by the motor. By doing so, we can evaluate whether the motor is producing the desired voltage or not, ensuring proper functionality and control.

We kept checking the boxes kept in 2072 where packages were delivered, and asked every source possible. We could not find the Microcontroller package, even though tracking information showed it had been delivered. 

## April 14th, 2023

Achyut and I were working on the display, and he was testing it with the Arduino with the help of a breadboard. The display was programmed easily with the help of the Adafruit libraries and the documentation they provided for their displays and I2C backpacks. 

I was trying to figure out the implementation of the microcontroller, with the help of the truth table provided in the design document. It was very comprehensive and I could draw up a rough outline of a flow chart. This helped me write the pseudo-code, and have it ready. 

![Truth Table](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Truth%20Table.png "Truth Table")


## April 18th, 2023

The testing of the display was completed, and it worked as we expected it to. This is when we were hopeless that our Microcontroller would reach us, we thought it was lost or misplaced. I sent an email to Jason about the order, and he encouraged us to use alternative methods to get everything to integrate and work.

We continued using the Arduino as we were before, and it was helpful since we could cover the time lost because of the delivery confusion. 
The microcontroller (taken from the Arduino) utilized the AnalogRead function to gather input from the battery voltage and subsequently provide an output indicating the battery level. 

In order to be able to do this, we had to figure out a mathematical equation which we could customize to correlate to the AnalogRead output to the battery percentage. AnalogRead is a function that can measure voltage values between 0 and 5 volts and convert them to numbers between 0 and 1023. We needed to figure out the voltage values for when the battery was at 0% and 100% and then map those values to a scale of 0 to 100. 0% would coincide with battery being drained, listed at 3.0V and 100% would be fully charged, listed at 4.2V. After doing this, we were able to display the battery level accurately on the screen.

![Battery at 0%](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Battery_0.png "Battery at 0%")

![Battery at 100%](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Battery_100.png "Battery at 100%")

## April 19th, 2023

The team wanted to make sure the hand-crank speed recommendation would appear on the display. This required an output from the Motor, but we were unable to make the Microcontroller read an input of greater than 5V without the sense signal circuit.

Meanwhile, Rubhav conducted sequential tests on the motor output, the linear regulator, and the Power MUX. Unfortunately, during this process, we encountered an issue where the Power MUX ended up being damaged. Upon further examination, we discovered that the 6V output from the linear regulator was too close to the maximum allowable voltage for the power mux. This proximity posed a risk, as minor variations in the crank speed could potentially cause the Power MUX  to fail. To address this concern, we made the decision to replace the existing linear regulator with a 5V variant. This helped the circuit have some headroom for the hand crank speed recommendation.

The linear regulator outputs 5V ± 0.05V in the nominal range of the motor, but when the voltage output drops below 6V, the output of the linear regulator would be below 4.9V ± 0.05V. This helped Achyut and I determine if the hand-crank is being cranked or not, and with the help of necessary boolean conditions, we were able to output to the display the recommended speed of crank for the user.

## April 20th, 2023

The team wanted to remove the Power MUX that had blown up, and replace the 6V linear regulator with the 5V one. 

We made the decision of sodlering on a new PCB, since the Power MUX proved very hard to remove, and this led to other parts of the board getting damaged inherently. 
The parts ordered were enough for us to solder another PCB board, we had many extra components and found it better to have a fresh start for testing. It was important to make sure the PCB traces were not damaged and the high voltage did not causes any other issues on the board. It was taking considerable time to test where the issues were, and starting fresh on a new board seemed like the better option to save time. Desoldering was also very difficult for some components, and we were not sure if overheating was occurring. Thus, the team decided to just solder on a new PCB.

Initially, we focused on soldering the Power MUX along with the necessary auxiliary components required for its proper functioning. To ensure that the soldering was done correctly, we conducted continuity checks to verify the success of each solder joint.

During the subsequent testing of the Power MUX, we encountered an unexpected issue. When we examined the IN1 input to VOUT using an oscilloscope, we noticed the presence of a triangle wave. This wave had a higher frequency than anticipated and exhibited a significantly lower voltage peak compared to the expected voltage output. This observation indicated that there was an anomaly in the behavior of the Power MUX, which required further investigation and troubleshooting.

At this point, we wanted help from Matthew, because the problem was becoming confusing and we were having a difficult time figuring out a solution.

## April 21st, 2023

The team had a Mock Demo, and we were informed about having printouts for the requirements and verification, the block diagram, and the high-level requirements. We also needed to prepare more, and have the setup ready since we did not have much time to demo.

After we were finished, Matthew was trying to figure out our issue.
 He suggested we check our connections and if they looked good, we should consider replacing the old Power MUX with a new one. This was concerning since we did not have many Power MUXes, which we realized was a mistake the team had made. We should have foreseen testing issues, and ordered extra. 

The final demo was near, and we had to prioritize getting some tangible results in order to gain points. We discussed what we would be showing during the demo, and we tested those requirements. The team recorded videos and we clicked pictures as proof to verify. We had a lot of data, and hence we made sure we provided this data to the Professor and TAs during the demo since we might not have time to demo every single test we performed.

Following the meeting, we conducted thorough testing of all the connections, ensuring that they were properly established and functioning as intended. After confirming the connections, we concluded that they were satisfactory.

Our plan was to proceed with the replacement of the Power MUX. This replacement would involve the installation of the last and final Power MUX available to us.

## April 22nd, 2023

The team began soldering on a fresh PCB again, with the new Power MUX. We were elated to conclude that the circuit worked as expected. 

The Power MUX issues were that the first one was blown during testing due to high voltage being passed through it, the second one proved to be faulty, and the third one worked as expected.

With this confirmation of a working circuit, we were able to solder all the remaining components onto the PCB.

## April 23rd, 2023

The team was confident because we had already tested and verified that all the parts of our project were functioning correctly. We were pleased to see that our product was working just as we had anticipated.

At this point, there were only two remaining components left to be soldered onto the PCB: the microcontroller from the Arduino and the display header. Once these final pieces were soldered in place, our project would be fully assembled and ready.

Later in the night, after we had a small break, Rubhav informed us about the short in our PCB. He said that the DC Power Supply kept going to Constant Current Mode which usually means there's a short circuit. He discovered that the input of the boost converter was directly connected to the PCB Ground, causing the short circuit.

The team's spirits were crushed since our demo was less than 15 hours, and from having a fully working product, we were back to square one of soldering and testing with not nearly enough time. During the night, we soldered all the components to a new PCB. We had to be careful when removing the Power MUX and other valuable components as we had limited supplies. When we attached the Power MUX and display to the new PCB, they did not function properly. We assumed that they were damaged due to the short circuit.

The team sent an email to Matthew updating him about our situation, and seeking help. We were trying to find alternative methods we could implement switching logic, without needing a Power MUX. The thought of using diodes was discussed, however, the ones in the supply center were not working in the circuit. At this point, the lack of sleep and the sudden panic had barred us from thinking straight and we were not able to think of any modifications. 

We wanted to use the Microcontroller as a MUX, and we would use two pins as inputs and another as an output. However, the current limit of the pins was 50mA and that meant we could not output power high enough to actually charge any device.

Rubhav and I were trying to think of more alternative methods, while Achyut worked on taking screenshots of the code of the display signals. 

![Charging AirPods](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Charging.png "Charging AirPods")

## April 24th, 2023

Regrettably, we were unable to complete the automated setup in time for the demonstration. As a result, we resorted to manually executing the switching logic using a breadboard. We informed Matthew and Professor Olga about the situation, providing them with an explanation of the challenges we faced.

I created a presentation with the images and screenshots we took since we would not have the time to present all the verifications. We wanted to portray solid data through images we had taken of results.
We were able to verify our requirements and show them working results for the product. Achyut's Airpods were shown to be charging, with the help of an image in the presentation. Rubhav's cable would light up every time the crank was turned, proving the working of the product. The product would have been enlcosed and compact, but the use of the breadboard caused us to keep the case open. 

I explained to Professor Olga why the product was not compact, and the compartments we had made for each component so the product would be seamless. 

Even though the team faced many challenges, we were able to deliver a great demonstration.

## April 26th, 2023

The team had to make slides for the Mock Presentation. The team gathered together for a meeting to discuss and prepare for the upcoming presentation. To ensure that our presentation was comprehensive and visually appealing, we decided to visit the lab. There, we took photographs of any remaining components, prototypes, or relevant materials that would enhance our presentation. By capturing these images, we aimed to provide a more thorough and professional overview of our project. We also spent time organizing and structuring the content of our presentation to ensure a coherent and effective delivery to our intended audience.

## April 27th, 2023

This was the day we delivered our Mock Presentation. The TA present said our technical content was long, and hence advised us to reduce the content that the audience did not want to see or would not be interested in or understand. The Communication Majors helped us improve our delivery, and gave us very positive feedback. We still had to add images of the final product, and add a working video of us cranking the product and showing the result.

We did not have to change much, only reduce the length of the presentation and keep it short and interesting.

## May 1st, 2023

With the final presentation approaching, the team dedicated time to refine and polish our slides. We carefully reviewed the content and incorporated any necessary changes based on feedback received during the Mock Demo session. In line with their recommendations, we decided to remove some of the more technical slides to ensure a smoother and more engaging flow for our presentation.

To enhance the visual appeal and effectiveness of our presentation, we included relevant videos that showcased the functionality and features of our project. These videos provided a dynamic and interactive element, allowing the audience to better understand the practical aspects of our work.

Additionally, we took a storytelling approach to our presentation, ensuring that the content flowed logically and captured the interest of our audience. By presenting our project as a narrative, we aimed to create a more engaging experience.

## May 3rd, 2023

The team worked on the Final Paper for the project. We had to form a comprehensive report explaining the design of the project and the problem it targeted. We included all our results, successes and challenges in this report. It served as a manual for our product, and a formal report for the working of our project.

![Final Product](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Final_product.jpg "Final Product")

![Final Internal View](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/shreyasi_ray/images/Internal_image.jpg "Final Internal View")




