## Lab Notebook - Rubhav Nayak

### Tuesday - 2/7/2023

 - Made Block Diagram
 - Spoke with our TA Matthew Qi about our project, advice to look into regulator control system as well as boost converter

### Thursday - 2/9/2023
 - Met with team to discuss Project Proposal
 - Modified Block Diagram to reflect the suggestions of TA
 - Wrote up on the Tolerances, Safety and Designed High-level Visual Aid for the proposal.

### Friday - 2/10/2023
 - Spoke with Machine Shop
 - We were advised to use a bigger, more reliable 12V geared motor to get our desired results.
 - Discussed with teammates and decided we would go with this for the sake of reliability since our project mainly consists of the electronics beyond the motor

### Monday - 2/13/2023
 - Spoke with Jack Blevins, our mentor for this project.
 - He suggested that we go with the Machine Shop's advice of using the bigger motor as well.
 - He also suggested a design where the hand crank is at the top of the product instead of on the side.

### Thursday - 2/16/2023
 - Edited Block Diagram to reflect the suggestions of Jack

### Friday - 2/17/2023
 - Met with TA, took into account the feedback regarding our MUX setup as well as the boost converter mechanism.
 - Matthew mentioned that if we use a switching converter instead of a linear regulator, we need to take into account high-frequency ripples, which may interfere with the rest of our circuitry.
 - He also advised us to completely understand the battery we're using when designing our boost converter, as a typical Li-ion battery can have a voltage ranging from 3.0 V to 4.2 V.

### Saturday - 2/18/2023
 - Updated block diagram with a system of relays
 ![Updated Block Diagram](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/Refined_Block.png "Updated Block Diagram")
 ![Relay Implementation](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/Multiplexing_Logic_Initial_Implementation.png "Relay Implementation")

### Tuesday - 2/21/2023
 - Updated our block diagram according to TA feedback, removed relay MUX setup in favour of power MUXes

### Wednesday - 2/22/2023
 - Met with Jack and presented him with our latest updates
 - Tested the voltage characteristics of the motor - we found out that the motor can generate 12V with around 60rpm, and 18V with around 100 rpm.
 - Looked up part numbers and tolerances for tolerance analysis
 - Wrote up the crank subsystem description for Design Document

### Thursday - 2/23/2023
 - Worked on the tolerance analysis for the Design Document
 - Revised the crank subsystem description - changed power mux part due to incompatibility
 - We looked into a voltage divider system to pull the voltage down before the power muxes, but decided against it as it was a power application. Instead, we just went for a 6V divider at the beginning since our losses turned out to be minimal.
 - Made a rough schematic of the crank subsystem in KiCAD
![Initial Schematic](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/Diagram_3_445.jpg "Initial Schematic")

### Friday - 2/24/2023
 - Met with TA to discuss various changes we could make to our PCB design as well as our design document.
 - He suggested that we could use a silicon controlled rectifier, or some MOSFET setup instead of a relay for our demultiplexing.
 - He also informed us about the places we lost points in Project Proposal, and the changes we need to consider when writing our design document, which included adding section numbers, more detailed tolerance analysis as well as expanding on verifications.

### Sunday - 2/26/2023
 - Designed PCB Schematic based on initial PCB Design and Design Doument and sent the schematic over to Achyut and Shreyasi to design the PCB.
![Initial PCB Idea and Connections](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/PCB_Layout_Rough.png "Rough PCB Layout")
 - We decided to use an external boost converter and battery charging module in the interest of battery safety. As a result, the circuit was simplified and the need for a second power mux was eliminated. The TP4056 charging module integrated well with the PCB idea we had in mind, as it was able to charge the battery through micro-USB, as well as an external DC supply, which in our case was the hand-crank module.

![TP4056 Charging Module](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/TP4056.jpg "TP4056 Charging Module")

![Alternative for MUX2](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/MUX2_Logic_External.png "Alternative for MUX2")

### Tuesday - 2/28/2023
 - Changed Power MUX from TPS2116 to TPS2115 in the schematic as the former was too small to solder by hand, which we noticed when designing our PCB.

### Sunday - 3/5/2023
 - Finalized PCB Schematic
 ![PCB Schematic](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/SchematicHiRes.png "Final Schematic") 

### Thursday - 3/9/2023
 - Worked on the Team Contract Evaluation
 - Made the initial cart for part orders from Mouser and DigiKey

### Friday - 3/10/2023
 - Met with TA to discuss part orders and our design document grade.
 - For our design document regrade, Matthew advised us to add more information to our block diagram, schedule, strengthen our table descriptions, as well as add some information on ethics and battery safety.
 - We also needed to add some more information to our R&V tables, especially ensuring that we test for tolerances. 

### 3/11/2023 - 3/19/2023
 - SPRING BREAK

### Friday - 3/24/2023
 - Matthew discussed power overheads with us. As our TPS2115 power mux has an absolute maximum rating of 6V, using a 6V regulator could lead to voltages above 6V as per the regulator's tolerance, which could cause our part to get damaged if there's not a significant load to pull down the voltage.
 - As a result we ordered an LM7805 Linear Regulator to be safe. This regulator regulates any input voltage from 6-20V down to 5V DC.

### Tuesday - 3/28/2023
- Worked on the Design Document Revision 
- Added more information to our tolerance analysis and strengthened our R&V requirements by adding tolerance values and more detailed verification tests.

### Wednesday - 3/29/2023
 - Worked on the Individual Progress Report

### Friday - 3/31/2023
 - We received our parts and discovered that we got some footprints wrong. All our components used 0805 footprints, but we ordered some that are 0402 as well as some that are 1206, which are both incompatible with our PCB.
 - Thus, we ordered those components with the correct footprints.

### Wednesday - 4/5/2023
 - Tested out the Boost Converter and Battery Module, both work to specification
 - We had to remove 2 resistors by design to get the converter to work

### Thursday - 4/6/2023
 - Received the Hand-Crank Box Assembly
 - Soldered the parts that were delivered to us

### Friday - 4/7/2023
 - Met with Matthew to discuss the requirements for mock demo and final demo.

### Wednesday - 4/12/2023
 - While examining the signals that are being sent/received by the MCU, we noticed that there was an issue with reading the voltage generated by our motor that could be safely detected by the Arduino, but Achyut found a solution that involved sensing the voltage from the linear regulator.
 
### Friday - 4/14/2023
 - Tested out our PCB, checked soldering and performed continuity tests
 - Noticed that our power mux was soldered incorrectly and corrected the orientation of it.
 - Removing the power mux was a pain, and I ended up bending some pins in the process. Removing the power mux is something we will avoid in the future, as the risk of damaging it is high, and even though it's more tedious to solder an entirely new PCB, it's less risky overall.
 - Spoke to Matthew about our project status, he told us to be wary of various programming issues, such as different grounds and noise affecting the flashing of the microcontroller unit.

### Wednesday - 4/19/2023
 - Performed tests using the hand crank and ensured that the default conditions were met (i.e. the hand-crank charges the battery by default, battery charges the device plugged into USB)
 - Noted down hand-crank characteristics
 ![Hand-Crank Characteristics](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/Motor_Characteristics.png "Hand-Crank Characteristics")
 - We noticed that the motor itself had an AC component, but because the peak-to-peak was little compared to the mean voltage, the ripple rejection circuitry on the LM7806 linear regulator was able to reject the ripples and give us a steady 6 VDC.
 - Performed tests on the USB output, and consequently, power mux, using the checks mentioned below:
 ![USB Output Tests](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/USB_Output_Test.png "USB Output Tests")
- While performing these tests, we noticed that the power mux blew up and was damaged. Upon checking the circuit, we noticed that the linear regulator had an output 6.3 V, and since there was no load, the voltage went to the power mux, and as it was above absolute ratings, the high-voltage damaged the power mux.
- To prevent such issues, we decided to switch to the LM7805CV, which was a linear regulator that regulated voltage down to 5V instead of 6V. 

### Thursday - 4/20/2023
 - We Removed the LM7806CV and soldered the LM7805CV linear regulator in its place.
 - We then replaced the damaged power mux with a fresh power mux, checking for contact and orientation as we were soldering.
 - The small size of the power mux gave us a lot of issues, to the point where we found it simpler to use a fresh PCB and move all the other components to the new PCB instead of removing the power mux.
 - After ensuring the power mux was soldered on correctly, we connected all the auxiliary components required for operation and tested the signals using the testing setup shown above.
 - While the signal for IN2 was being output correctly, the signal from IN1 was a high-frequency triangle wave that was less than half the voltage of the input signal. 
 - Assuming it was an issue with our basic operating circuit, we re-checked all components but found no fault.
 - We decided to ask our TA tomorrow during the mock demo for any advice.

### Friday - 4/21/2023
  - MOCK DEMO 
  - Upon asking Matthew, he recommended re-checking our connections and if everything looks right, then to consider replacing the power mux.
  - Since our connections looked right, we decided to replace the power mux.

### Saturday - 4/22/2023
 - We soldered our last power mux to another PCB and connected the auxiliary components to ensure proper operation.
 - Upon testing with low-voltage signals as described in the USB Output Tests, the tests were successful and we could start soldering other components required to test the electromechanical subsystem

### Sunday - 4/23/2023
 - We tested the electromechanical subsystem as per the R&V mentioned in the design document, and it passed all tests successfully.
 - As the electromechnanical subsystem was working fine, we soldered the display connector to the PCB and connected the display.
 - I then wanted to test if the display was getting power correctly, as well as collect some information regarding the swithcing time of the power mux.
 - However, when we connected our inputs to the DC power supply and turned it on, within a few seconds there was smoke from the power mux and the display suddenly shut off.
 - The DC power supply was in constant current mode, which indicates that there was a short somewhere in our PCB, and the constant current mode meant that we were supplying 5A to our PCB, which is a lot more than our components can safely handle.
 - Upon further testing, we noticed that the PCB ground was shorted to the input from the boost converter.
 - When we inspected the circuit, we were unable to find any signs of shorting caused by the components, so we assumed it was an internal short within the PCB. This was a plausible explanation as we noticed that a lot of our traces between live and ground were very close to each other, and there may have been some arcing caused by some PSU transient.
 - Regardless, since that PCB contained our last power mux and display module, it meant that we had to proceed without those components.

### Monday - 4/24/2023
 - FINAL DEMO 
 - Since we no longer had a working power mux or display, we decided to come up with a plan to implement the functionality of those components externally.
 - Achyut managed to show the microcontroller's functionality using print statements, and I tried to work on implementing our muxing logic with a PCB.
 - An easy solution would have been to use the initial muxing logic idea mentioned in the Relay Implementation figure, however we only had one relay, so we couldn't use this idea.
 - Upon Matthew's advice, we tried using MOSFETs to build a multiplexing circuit, but it was finicky and required a higher voltage to toggle, and with other preparation that we needed to do for our final demo, we decided to just manually switch our signals for demonstration purposes.

### Wednesday - 4/26/2023
- Met with the team to work on the presentation.

### Friday - 4/28/2023
- Met with the team to record a video of our project for extra credit.
- However, when preparing the circuit for demonstration and asking Jason for advice regarding the same, we mentioned our short and he mentioned that PCB internal shorts are very rare, and there might've been bridging caused by either our power mux or our display connector.
- Upon closer examination, we discovered that there was some bridging between the ground and power of the display connector, which actually caused our short.
- We then updated our presentation to reflect this information.
- Below is our final product assembled
![Final Product](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/rubhav_nayak/diagrams/Final_Product.jpeg "Final Product")

### Monday - 5/1/2023
- Met with the team to rehearse and finalize our presentation
- We made some more changes to make the presentation shorter.

### Wednesday - 5/3/2023
- Met with the team to finish final paper.

