## 01/17/2023
This was the first team meeting we had. 
We met up virtually to discuss different ideas that we could potentially pursue for this course. 

I proposed a [LiDAR-assisted device for helping Visually Impaired people. ](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=71714)
The project was inspired by braille devices that have allowed visually impaired people to read, so I wanted to create a self contained device that would allow the user to get critical information about their surroundings from this product. The way the product would work is, it would be a piece of eye wear that would have LiDAR sensors that would then relay information to the microcontroller where the MCU would process the information and through sounds and vibrations tell the user that there is an obstacle approaching or the size and distance of the obstacle.

We really liked the idea and I posted it that day itself and we discussed the practicality as well as the presence of competitors. 
We also discussed the other ideas of the hand cranks charger and the game controller ideas. 

## 01/22/2023
Between the last meeting and today's meeting, Shreyasi and Rubhav had come up with their own ideas. Rubhav disucssing a wireless controller that could be connected a larger group of devices rather than limited, and Shreyasi proposing a Portable Hand Crank Charger.

We decided to go with Shreyasi's project idea of the [Hand Crank Charger](https://courses.engr.illinois.edu/ece445/pace/view-topic.asp?id=72429). 
We especially liked the idea as it tackles a problem that not many people typically look at. Emergency situtaions and times of need are often overlooked and the heavy reliance on technology is something we see all around us, so we thought this brings those two points together and makes a product that could be viable for production and sale.

We then went ahead and wrote up the Request For Approval (RFA) document where we highlighted the problem statement, how our proposed product plans to solve that problem efficiently and cheaply, and parts we think will be needed to build it. 
One of the more important aspects of this discussion was the component selection. Given that we had various requirements, such as it needs to be cheap, efficient, portable and automated we had specific requriements for the parts we chose. 
For example:
- Motor: We wanted the motor to be light weight small enough to be compact and have a high enough voltage where the motor could comfortably supply a steady 5V output to the USB Output.
- Crank: We wanted to build a custom hand crank that would fold on itself reducing the overall packaged length, yet maintaining a long distance between the axis of rotation and the point of applied force. $T=Fd$
By increasing the distance we can reduce the force the user would need to apply to get the same desired torque output.

Other parts were also chosen while keeping these design considerations in mind, but given that the components most vital to our product were the hand crank and the motor we specifically emphasized on them during this meeting. 

## 01/24/2023
We met for a brief time on this day to finalise the RFA and complete it before we submit. We worked on the various subsystems we wanted to have and also the criterion for success. We also went over in more details how the 2 subsystems are differentiated and their intended usecases. We also went ahead and drew up a rough sketch of what we wanted the product to look like.
![Initial Rough Design of Product](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/rough_design.png)
As you can see, I had envisioned a small box with a USB A cut out on the side where the user could plug in their device, and on the other side there would be the hand crank. On the top of the product is where our display would be mounted for easy understanding of battery percentage. 


## 02/04/2023
We met up to discuss the Project Proposal. This was more hands-on as we needed to discuss the exact parts of the project and create the block diagram. We came up with a rough Block Diagram (image attached) and we also discussed the key points of our proposal today. We started off by first looking at what is required in the project proposal and worked on each point sequentially. We discussed what set of features we want our product to have. For example, we wanted the product to also have power bank like functionality and so we incorporated a 1000mAh battery into our proposal as we think having the ability to charge a battery increases the usability of the product.

![Rough Block Diagram](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/rough_block.png)

## 02/07/2023
Today we had our TA meeting for the first time, we discussed the block diagram in detail, we also discussed pros and cons of which motor we are using, and discussed the variable voltage divider. This was because the Variable Voltage Divider is an extremely critical part of our project, and making that work will be a large part of our project. 
More specifically, while discussing the motor choices, we mentioned to him that we were contemplating using either a Brushed DC Motor or a Brushless DC Motor. The considerations were presented:

| Characteristics | Brushed Motor | Brushless Motor | 
| --------------- | ------------- | --------------- |
| Longevity | Short lifetime | Longer lifetime | 
| Efficiency | Lower | Higher |
| Noise Level | Louder | Quieter |
| Cost | Cheaper | Expensive | 

As it is very evident, a brushless motor is significantly better in terms of use cases, but given that is expensive and we wanted to make the product more affordable, we were strongly considering a brushed motor only. 
These were some of the notes we took during the quick discussion. 

![TA Meeting Notes](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/TA_meeting.png "TA Meeting Notes")

## 02/09/2023
We met up to finalize the project proposal, we created the CAD models for the rough design of our product, and also further discussed intricacies in the design.

![Product CAD Auxiliary View 1](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/cad_roughAV1.jpg "Product CAD Auxiliary View 1")

![Product CAD Auxiliary View 2](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/cad_roughAV2.jpg "Product CAD Auxiliary View 2")


This CAD design is what we originally had planned for our idea, this is also what we will show the machine shop when we approach them with our product. The CAD models where created in Autodesk Fusion 360. 
- The model showcases the recess in the top of the product where the display would be fitted, then it also shows 
- The model also has the hand crank on the right side of the prodcut. 
- The model has the respective cut outs for the USB A Output and Micro-USB Input.
- We also included screw holes on the base plate for screwing the two parts together. 


## 02/10/2023
We went to the machine shop and met with them for an initial discussion regarding the needs of our project. The discussion was mainly based around the CAD Models and what we had discussed the previous day. 

We also wanted their input on the motor as they would have the industry knowledge that we would need to make an informed decision. While talking to Greg Bennett, he mentioned that the small Pololu motors we were considering, more specifically [Pololu Motors](https://www.pololu.com/file/0J1487/pololu-micro-metal-gearmotors_rev-5-1.pdf) were too fragile. He mentioned that he has used those motors before and they have plastic gears inside which are good when driven from the electric side to the mechanical side, but they have a tendency to crack when driven from mechanical to electric side. He also mentioned for the purpose he has a good brushed motor that he could lend us. This motor, although large, had metal gears which is what we needs for reliable use from crank to electric side. Therefore we will perform tests and evaluate if we can use the motor.

As for the physical design, he mentioned that [Polycase.com](https://www.polycase.com/) is a good option for ordering ABS Plastic boxes which would be good enough for the prototyping phase. 

## 02/13/2023
We met Jack Blevins today to discuss our project, he brought up some very useful points regarding the hand crank placement and also using the 12V motor to charge the battery and the device simultaneously. This would help prevent unecessary power wastage as well. 
We met Jack Blevins today to discuss our project. He brought up quite a few good points of consideration, these were:
- He mentioned the hand crank placement should be something that we should strongly look into as the product should be usable in most if not all environments, so we should be able to prop the product sideways so it can be used by rotating it in the horizontal plane. 
- He also suggested that we can potentially split the 12V output of the Motor and charge both the USB Output and the Battery simultaneously.

The former was an easy implementation as we just had to move around our USB input and output to another side and make one other side a flat base. 
The latter was an interesting design implementation to consider. 


## 02/16/2023
Rubhav and I discussed revisions to the block diagram, and also how we can potentially split the 12V output to charge the battery and USB out simultaneously. 

We looked at the voltage divider circuitry that we would have to incorporate to charge both battery and USB output simultaneously but we soon realised that the current division would cause there to be issues charging the USB Output as it would be too low for any modern device to charge. Furthermore, the other issue with this is that the user would have to ensure that they crank fast enough consistently for both to charge, and if they were not cranking optimally, then neither battery nor USB would charge, therefore we thought for user comfort we would simply use a Relay to decide where the power would flow. 

## 02/20/2023
Started working on the design document. After extensive research into Power MUXes and also different relay set ups we concluded on what we wanted in our project, and chose to use two POWER MUXs and one Relay. This will give us automated functionality. 
![Final Block Diagram](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Final_Block_Diagram.png "Final Block Diagram")

## 02/21/2023
Looked at the different parts to account for various losses for tolerance analysis. This also helped us get a gauge of the parts we needed. We noticed that we need a Diode which typically has voltage drop of roughly 1V so we needed our linear regulator to output atleast 6V. After this research we finalised the parts we will use and came up with the comprehensive bill of materials.
![Bill of Materials](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/BillOfMaterials.png "Bill of Materials")

## 02/22/2023
This was our second meeting with Jack, this was more of a finalize our diagram meeting. 

We discussed any potential hiccups in our strategy and also the use of Power MUXs. We discussed the thresholds of the power mux we were considering the **Texas Instruments TPS2116** and it appeared to be perfect for our project as it would handle the switching logic quickly and the IC itself had very low voltage drop across the input and outputs.

We also discussed the microcontroller and the display. 

While disucssing the microcontroller we discussed the language we would use and how C++ is the most commonly used language therefore it would be best to use it as it would have the widest documentation and online support. W

We also discussed the display and how we may have to look carefully at how many GPIO pins we would need to output to the display as typically each 7 segment digit requires 8 pins (7 for segments, 1 for decimal point) and therefore considered looking at alternatives.

This is when I mentioned that I was looking at ordering a display with the I2C Backpack so that we can reduce the number of pins required down to 4, letting us use the other GPIO pins for other parts of our design. 


## 02/23/2023
I wanted to create a truth table that would better explain the program that we would write for the microcontroller. Since the program itself would not need to be that extensive, we needed to mainly understand all the different states that are possible for our program to exist in.

![Truth Table](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Truth_Table.png "Truth Table")

As you can see from this truth table, it has 5 inputs:
1. Hand Crank - Set to 1 when Hand Crank is being cranked, 0 when not
2. USB In - 1 when Micro USB Input is connected, 0 when not
3. USB Out - 1 when a device is connected to the USB Output, 0 when not
4. Battery Low - 1 when the internal battery is low (< less than 15%), 0 when > 15%
5. Battery High - 1 when the the battery is fully charged, 0 when not

This really helped us know when we should send power to which component and from which source.


## 02/24/2023
We had our TA meeting today where we disucssed updates to our project, and the next steps towards it. We discussed briefly what we were lacking in our Project Proposal and how we needed to add more information. 

Furthermore, we discussed specific parts like the use of:
- Ceramic Capacitors
- Opto Coupler for the sense signals to the Power MUXs and Relay
- SCR as an alternative to the Relay. 

Matthew also brought up a good point about potentially using an off-the-shelf part for the boost converter due to safety concerns. So we decided on using the AdaFruit Powerboost 1000C.

![TA Meeting 02/24](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/TA_meeting_02-24.png "TA Meeting")


## 02/27/2023

We had our Design Review today with our TA, Matthew Qi, and also Professor Olga. During the presentation we discussed our design document going into detail about the high level requirements, how we plan to verify the various requirements we have and also went over each subsystem. This was very insightful as we got great input about each requirement and how we should write up each test procedure clearly which would help us to perform said tests in the future. Towards the end we also discussed the validity of our product, how it compares with others existing currently and its intended use cases keeping in mind the perspective of the user rather than the creators. 

## 02/28/2023
After my teammates completed the PCB Schematic, I worked on the PCB Design and laying out the parts on KiCAD to get a good idea of our PCB and understand how we want to lay each component. This was important as it helped us plan the overall design of our product (where the USB, motor, display will be) and also helped us gauge the final size of our product. 

This was also something I wanted to do to ensure that it was easy to debug and unit test each part of our PCB. I kept each main part in its own unique area. 

![Labaled PCB](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/labeled_pcb.jpg "Labeled PCB")

As you can see in the PCB design, we have layed out the parts according to functionality as follows:
| Placement | Portion | 
| --------- | ------- |
| Top Left | USB Output | 
| Bottom Left | Motor and USB Input | 
| Top | Power (Boost Converter) |
| Right | Programming and Display | 
| Middle | Switching Logic (MCU, Power Mux, Relay) |

While designing the PCB, I noticed that the power mux we were using, the [**Texas Instruments TPS 2116**](https://www.ti.com/product/TPS2116#pps) appeared far too small compared to other ICs and standard parts we are used to seeing. So I referred to the [TI TPS2116 Datasheet](https://www.ti.com/lit/ds/symlink/tps2116.pdf?ts=1683244260958&ref_url=https%253A%252F%252Fwww.google.com%252F) where I noticed that the overall package was (1.3mm x 2.2mm) with 8 pins in a SOT Package type which would have been really hard for us to solder given our limited experience. 

So we looked at an alternative to this power mux as this was an important component and we wanted to ensure the part works well, so we found the older version of the TI TPS 2116, the [**Texas Instruments TPS 2115**](https://www.ti.com/product/TPS2115#pps).
This being an older component was bigger and had larger pins in the TSSOP package type with its dimensions being 3mm x 6.4mm, which was still very small considering there were 8 pins, but still something we could work with. 

So we revised the PCB Design to incorporate it and we completed the PCB design that day.

## 03/02/2023
While looking at parts, we noticed that the Powerboost 1000C by AdaFruit was out of stock so we had to find a replacement that would take care of the desired functionality. 

So we tried finding parts on Mouser and Digikey, but unfortunately most parts were either out of stock or backordered, therefore we went to Amazon. Here we found a product by MakerFocus. This product incorporated the [NanJing Top Power TP4056](https://dlnmh9ip6v2uc.cloudfront.net/datasheets/Prototyping/TP4056.pdf), which is the universal constant-current/constant-voltage IC that is used in most battery charging modules. 

[The Product](https://www.amazon.com/Makerfocus-Charging-Lithium-Battery-Protection/dp/B071RG4YWM/ref=as_li_ss_tl?dchild=1&keywords=TP4056&qid=1593227138&sr=8-3&linkCode=ll1&tag=circbasi-20&linkId=957634db00eebaef6169a13464f34088&language=en_US) worked perfectly for us as it would essentially handle the functionality of choosing what charges the battery between the hand crank and the MicroUSB input taking care of our Power MUX2 functionality. 

This was especially useful as being off the shelf and also incorporating an IC dedicated to charging would make our product much more safer and take care of battery charging protections before our circuit would even be able to. 


## 03/06/2023

After making sure that our PCB looked good in terms of design logic, we went ahead and silkscreened all the necessary values on the PCB rather than putting generic Resistor and Capacitor labels as we felt this would make our lives easier while soldering. We then reviewed our PCB through PCBWay's online verification system, sending the gerber files over and getting the approval from PCBWay. We then went ahead and sent Michael the PCB files so we could get them in the first round of shipments.

![PCB Layout](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Final_PCB.png "PCB Layout")

![PCB 3D Auxiliary View](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/PCB_3D_AV.png "PCB 3D Auxiliary View")

## 03/15/2023
Over spring break, we did less work, but Shreyasi and I worked on finalizing the parts. We focussed on looking for parts that would were in stock and if we needed specialized parts that were out of stock we looked at alternative retailers or alternative products. 

## 03/18/2023
Shreyasi and Rubhav together looked at the parts we need to order and put in the purchase order a few days after. 

## 03/31/2023

During the TA Meeting this week, we discussed the parts we ordered and Matthew mentioned that we need to make sure all of our components have the correct footprints. More specifically we needed to ensure that our resistors and capacitors have the right footprints, this is because the other parts are typically specific and their footprints are unique, but in the case of resistors and capacitors they can have different footprints and different sizes. 

While designing the Schematic and the PCB CAD, we made sure to have the 0805 footprints for the resistors and capacitors as this struck a good balance between compactness and also ease to solder. But when we reviewed the order that was placed, we noticed that certain resistors and capacitors were incorrectly ordered, with some being 0402 (far too small) and some being (1206) too big for our footprints. 

Therefore we worked on ordering the parts again, this time making sure that the footprints of all parts matched that of the PCB order that we placed. 

## 04/02/2023

Of the two orders we had places, Mouser had arrived on time but DigiKey was delayed.

The Mouser cart contained a lot of the resistors and capacitors most of the correct footprints but some incorrect, and also contained the linear regulator and the relay. Given that we were considering using the stencil, we could not solder any of the components until we get the power mux which was in the DigiKey order, therefore we decided to perform testing on individual components on a breadboard. 

The DigiKey cart was delayed due to unexpected reasons, and it had been almost 2 weeks, so we went to the Business Administration office and spoke to them. It turns out that there were certain parts in the Digikey order that went out of stock leading the entire cart to be delyed. Therefore we decided to reorder the parts that were available. 

## 04/04/2023 
We went to the machine shop to finalize the entire product casing and get the ball rolling on building out the case and hand crank. We met with Gregg Bennett and also with Skee G. Aldrich and discussed the product. They both were extremely helpful and we gave them the motor and gave them the rough dimensions of our major components like PCB, daughter boards and Display.


While discussing the product we had many design considerations to discuss like the size of the overall product, the placement of the different parts and also length of the crank. 

These all came as quality of life improvements that we wished to make so our product is more feasible. One such thing was keeping the dipsplay aligned with the natural way most people would use the product. Given that most of the population is right-handed, we made the product with them as the average consumer. So if you are cranking the crank with your right hand the display is right side up and the USB ports are towards you. The crank length was decided at 5 inches as this provides the perfect balance between usability and practicality. This was becuase it reduces the force required but also doesnt make it too long. We also reinforced the motor mount with Metal to ensure the torque doesnt break the plastic case. 

## 04/05/2023
We met with Skee to give him the PCB, and also sent him the Datasheets that included all the accurate dimensions. We also collected our parts and confirmed that all the parts that we ordered were correct. 
We performed very simple unit testing on the resistors and Capacitors that were the right size (0805) as we had heard from some groups that certain resistors were not working for them.

At night we went back to the lab and began unit testing our other components.
By then the Power MUX had not arrived, so I first tested the Relay using the Digital Multimeter (DMM) and was happy to see that the relay works exactly as we expected it to. It was also extremely fast with its switching when there is current flow to the control pins mitigating any ripple that may come by slow and not controlled switching. I tested the Relay at a variety of Voltage values from 3V to 6V and it worked perfectly at all cases. The test was performed by plugging the Relay into a breadboard, and checking the output Voltage on the pins as per the given input signal. The way the relay works is there is a Normally Open (NO) and Normally Closed (NC) output, input rails and the switching logic. When we supplied power to Pins 1 and 6 (from 3V to 6V) we saw that Pin 3 (NC) would get the output with minimal voltage drop (<0.1V). We then would supply power to pin 2 and GND pin 5 and see that Pin 4 (NO) would get power and Pin 3 would not. Therefore the relay was working as we wanted. We also were confident that we could solder the Power MUX without the stencil as the stencil order was delayed. So we soldered the diode, and some of the resistors and capacitors that would not make it harder to solder other parts. 

![Relay Test](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Relay_test.jpg "Relay Test")

We see in this image that when we connect an oscilloscope to the Normally Open pin, when we supply power to the Pin 2, within 4ms it goes from 0V to 5V.


## 04/06/2023
In the afternoon we went to the machine shop and met with Skee. He had built out the complete hand crank and casing as per our discussion. 

![Build](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Build.jpg "Build")

The machine shop also mounted the motor on a metal plate to ensure that the motor torque would not crack the plastic. Also the hand crank knob was free rolling so it wouldn't bother the wrist while cracking.

Later in the day we went to the Senior Design Lab and soldered the Seven Segment Display to the I2C Backpack and other parts like the connectors and finished a lot of the PCB soldering. 

Since we completed the soldering of almost all the parts we wanted to make sure all the parts were soldered correctly. We ran continuity checks on the parts using the Digital Multimeter ensuring the connections were proper and the only component left to be soldered was the microcontroller.

The microcontroller was delayed due to it also being back ordered which is why we decided to also purchase an [Arduino Uno Rev3](https://store.arduino.cc/products/arduino-uno-rev3) which has a removable ATMega328p which is the microcontroller we are using. 



## 04/07/2023
We met with Matthew and discussed our overall process, we will now work on parts seperately with Rubhav focussing on testing the PCB overall, and me and Shreyasi working on programming the microcontroller. 

## 04/10/2023
I started programming the arduino to get the basic code running on an arduino so we can test with ease by connecting the arduino to a breadboard.
My first thing to test is the display and then will work on understanding how to access pin values and output to pin values. 

## 04/12/2023
One issue we noticed was that we have no trace from the motor to the microcontroller as we were not sure how to get the sense signal without passing too high of a voltage (>6V) so we decided that we will use resistors and use wires to get that value. 

But the same day we realised that might cause issues with the flow of current,so we instead soldered a wire from the output of the 5V linear regulator to an empty analog pin on the microcontroller. This will allow me to read the value that is being passed in and evaluate if the motor is generating voltage or not. 

## 04/14/2023 
At night, I went to the lab and got the display. Since I was testing it with the arduino and I had a breadboard and wirekit, I was able to program the display at home and test it out. 
The display was very easy to program as Adafruit has great libraries and [documentation](https://learn.adafruit.com/adafruit-led-backpack/0-dot-56-seven-segment-backpack-arduino-setup) for their displays especially the ones with the I2C Backpacks.

I also helped Rubhav solder the Power Mux after we noticed that it was soldered in the wrong orientation. 


## 04/18/2023
Completed testing of the display and it worked exactly as expected. The microcontroller used AnalogRead to read input from the battery voltage and output the battery level. 

To do this we had to write a custom mathematical equation that would correlate the AnalogRead output to a battery percentage. This is because AnalogRead can read any voltage value from 0V to 5V and it maps them from 0 to 1023 values. So we had to find the values for 3V (0% of battery) and for 4.2V (100% of battery) and map those values to 0 to 100.
We were able to do this successfully and get the battery output correctly on the display. 


!["Battery at 0%"](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Battery_0.png "Battery at 0%")

!["Battery at 50%"](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Battery_50.png "Battery at 50%")

!["Battery at 100%"](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Battery_100.png "Battery at 100%")


## 04/19/2023
The next step for programming was the Hand Crank speed recommendation that we wanted to show on the display.

For this to work, we needed an output from the Motor, but as mentioned earlier we could not have the microcontroller read an input of greater than 5V without the sense signal circuit.

While this was going on, Rubhav was testing the motor output, the linear regulator and the power mux in order and unfortunately we ended up blowing the Power Mux. Upon testing, we realised that the linear regulator's 6V output was too close to the Voltage_max for the power mux, and this could have caused the power mux to blow up due to minor invariances that occur due to variable crank speed. This caused us to change the linear regulator for a 5V one. 

This actually made my job easier as now I had some headroom for the hand crank speed recommendation.

So we decided to read the output of the linear regulator as we noticed that the linear regulator outputs 5V ± 0.05V in the nominal range of the motor, but when the voltage output of the motor dropped below 6V, the output of the linear regulator would be less thab 4.9V ± 0.05V. Therefore Shreyasi and I used this to understand if the Hand Crank is being turned enough or not, and we used the necessary boolean conditions to output to the display the hand crank speed recommendation. 

![Fast Recommendation](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Fast.png "Fast Recommendation")

![Slow Recommendation](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Slow.png "Slow Recommendation")


## 04/20/2023
Today we came in with quite a few goals in mind. 

We wanted to remove the old blown up power mux and the 6V linear regulator and put in the new parts namely a new power mux and the 5V linear regulator.  

We decided to start on a fresh PCB.
This was done for a couple of reasons:
- The power mux was very hard to remove causing many issues
- We had many extras for the other componenets anyway so we didnt need to reuse parts
- We wanted to make sure the PCB traces were all still intact and the higher voltage didnt cause any issues
- Just thought from a mental perspective starting fresh is far easier than desoldering and resoldering. 

We first soldered on only the power mux and the Auxiliary componenets needed for its complete functionality, starting off with continuity checks to ensure all the solder points were successful.
This time when we went to test the Power Mux we noticed that there was an unexpected triangle wave coming on the oscilloscope when we tested the IN1 input to VOUT. More specifically it was a high-frequency triangle wave with a significantly lower voltage peak than the expected voltage output. 

Since we were a bit confused why it was happening, we decided to wait for the next TA meeting that was the next day and ask Matthew why it was occurring. 

## 04/21/2023
During the Mock Demo, we asked Matthew why the issue was occurring, and he suggested we once check all of our connections again and if they look good consider replacing the power mux for a new one. 

During the meeting we discussed a bit more about how we would be demoing during the final demonstration and what requirements we would be showing we successfully tested and also what tests we would have videos and images to verify. 

After the meeting, we tested all the connections and we confirmed that all the connections were good, so now tomorrow we will replace the power mux for the last and final power mux we have. 

## 04/22/2023
We once again started on a fresh PCB and soldered the power mux and the components needed for it to work successfully and fortunately it worked exactly as we needed it to. 
So to summarize the power mux issues, of the three that we had:
1. We blew up the first one by passing in too high of a voltage
2. Appears to be a faulty power mux
3. Works as expected

Given that we had successfully tested the other components, we went ahead and soldered all the other pieces on the new PCB.

## 04/23/2023 and 04/24/2023
During the afternoon, we came to the lab with confidence that all of our parts work successfully and our product works as expected. The only parts left to solder were the microcontroller from the arduino and the display header on the PCB. 

I soldered the display header on the PCB after which Rubhav wanted to collect some data for the demonstration. 

After eating dinner, I came back to the lab when Rubhav mentioned that our PCB has a short. He mentioned that the DC Power Supply was consistently going to Constant Current Mode and this typically signals a short is present. He found that the input of the boost converter is shorted to the PCB Ground. 

This caused us to have a small panic attack as our demo was in less than 15 hours and we did not have a working PCB.
Through the night we completely resoldered all the components to a new PCB which included carefully desoldering the power mux and other valuable components that we running low on. 

*This is also why the line between the two days is blurry*

When we put the power mux and display to the new PCB we saw that they were not working as expected meaning that they probably got damaged in the short.

We emailed Matthew just to let him know what the issue was and tried to find ways we could implement the switching logic without the need for the power mux. 

We thought of using diodes but there were issues with the implementation as the diodes present in the supply shop were not working as we wanted them to, also since we were running on no sleep this could have led us to not have a clear headspace which could have potentially led us to not think of obvious implementations. We thought of using the microcontroller as a mux by using two pins as inputs and another as an output, but the current limit on the pins of 50mA meant that we could not output a high enough power rating to actually charge anything. 

While Rubhav and Shreyasi thought of a workaround, I wanted to take screenshots of the code outputting the display signals correctly, so I decided to print to serial monitor and just use that as a way to verify the display worked. 

Unfortunately we were unable to get the automated setup working in time for the demo, so we manually performed switching logic on a breadboard and explained the situtaion to Matthew and Professor Olga. 

## 04/26/2023
The team met up to discuss the presentation.
We went to the lab to take pictures of anything that was left and formalize the entire presentation. 

## 04/28/2023
We met up before the presentation to rehearse and have a good coherent presentation. 

We recieved very positive feedback from the TAs (both ECE and Comms) mentioning our presentation style and confidence showcased dedication to the project and also made them feel very interested in the project. The only critique they all universally mentioned was that we were too technical and perhaps we should remove some of the tests we performed and make it more of a story and talk about the journey through the class.

Later that day we wanted to take a video for the final presentation and also so we could upload a video demo. While working with the product, I spoke to Jason about the video demo and also mentioned to him our situation with regards to the internal PCB short. He mentioned those shouldnt be happening as they are manufactured to a very high degree of perfection, and therefore we examined our PCB carefully realising that the display connector had a short present. This makes sense as the issues on the 23rd occurred right after I connected the display header. 

We made modifications to our presentation to reflect this new information.

## 05/01/2023
We worked on the final presentation making the changes needed, adding all the necessary videos. As per the recommendations of the TAs from the Mock Demo we removed a few of the technical slides and made the presentation have a more story like flow. 

## 05/03/2023
We worked on the final paper.

## Our Final Working Product
To see a video of our product working please go to the [following youtube link](https://www.youtube.com/embed/Tb4moAehst0)



Below attached are images of the final product working as expected. 
![Final Product](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Final_product.jpg "Final Product")

![Final Product Internal](https://gitlab.engr.illinois.edu/achyuta2/ece445_aa_sr_rn/-/raw/main/notebooks/achyut_agarwal/images/Internal_image.jpg "Final Product Internal")



